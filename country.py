# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond import backend
from trytond.model import ModelSQL, ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval


class Country(metaclass=PoolMeta):
    __name__ = 'country.country'
    tax_zone = fields.Many2One('account.tax.zone', 'Tax Zone')
