# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool

from . import account, country, purchase, sale

__all__ = ['register']


def register():
    Pool.register(
        country.Country,
        account.TaxZone,
        account.TaxRuleLineTemplate,
        account.TaxRuleLine,
        module='account_tax_rule_zone', type_='model')
    Pool.register(
        account.InvoiceLine,
        depends=['account_invoice'],
        module='account_tax_rule_zone', type_='model')
    Pool.register(
        sale.Sale,
        sale.SaleLine,
        depends=['sale'],
        module='account_tax_rule_zone', type_='model')
    Pool.register(
        purchase.PurchaseLine,
        depends=['purchase'],
        module='account_tax_rule_zone', type_='model')
